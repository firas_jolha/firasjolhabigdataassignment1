name := "FirasJolhaBigDataAssignment1"

version := "0.1"

scalaVersion := "2.12.10"

val sparkVersion = "3.0.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
//libraryDependencies += "com.outr" %% "hasher" % "1.2.2"
