import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SaveMode, SparkSession}

case object Indexer {

  // Set Logger
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  // Start Spark Engine
  val spark: SparkSession = spinUpSpark()

  // Specify the input path
  var URI = "hdfs://namenode:9000/EnWikiMedium/*" // default

  // total number of articles
  var totalArticles: Long = 0

  // You can specify to save it locally or HDFS
  val parentSavePath = ""

  // Path for persisting statistics
  val docWordFreqSavePath:String = parentSavePath + "tfidf"

  // Path for persisting TF-IDF weights
  val indexingSavePath:String = parentSavePath + "indexing"

  // Path for persisting statistics for viuslization purposes
  val viewIndexingSavePath:String = parentSavePath + "view_indexing"

  // Path for persisting articles info including article id, url and title
  val articleInfoSavePath:String = parentSavePath + "articles_info"

  /**
   * Th entry point to the Indexer
   * @param args program arguments that may include input path and savepath
   */
  def main(args: Array[String]): Unit = {

    println("=================\nHello to my Text Indexer\nAuthor: Firas Jolha :)\n=================")
    println("You could specify the input path if you would like")
    // You could specify the input path through program arguments
    // by default hdfs://namenode:9000/EnWikiMedium/*
    if (args.length > 0) // You can specify the input path through program arguments
      URI = args(0)

    println("The path of files to be indexed is ==> " + URI)

    println("Reading articles.....")
    val articleObject: RDD[(Int, String, String, String)] = readInput(URI).persist() // read All Articles

    val textCorpus = articleObject.map(a => (a._1, a._4)) // (id, text)

    // for persisting purposes
    val articlesInfo: RDD[(Int, String, String)] =
      articleObject.map(d => (d._1, d._2, d._3)).persist() // (id, title, url)

    totalArticles = articlesInfo.count()
    println("Number of found articles to be indexed is ==> " + totalArticles)


    println("Extracting words of each article.....")
    val wordsByArticle: RDD[(Int, Array[String])] = enumerateWordsByArticle(textCorpus).persist() // all words
    // Getting the words of each article as RDD
    // Int ==> article id
    // Array[String] ==> Array of words

    val totalWords = wordsByArticle.map(_._2.length).sum().toInt
    println("Total words is ==> " + totalWords)
    println("Average length of articles is ==> " + totalWords.toDouble / totalArticles)

    println("Counting words for each article.....")
    val wordFreqs: RDD[((Int, String), Int)] = getWordFreq(wordsByArticle)

    println("Counting Total words for each article.....")
    val totalWordsPerArticle: RDD[((Int, String), (Int, Int))] = getTotalWordsPerArticle(wordFreqs)
    val vocabularySize = wordFreqs.map(_._1._2)
        .distinct()
        .count()
    println("Vocabulary size is ==> " + vocabularySize)

    println("Counting documents for each term.....")
    val docFreqs: RDD[((String, Int), (Int, Int, Int))] = getArticleFreqPerTerm(totalWordsPerArticle).persist()

    println("Computing TF-IDF weights.....")
    val tfidfs: RDD[((String, Int), Double)] = getTFIDFWeights(docFreqs).persist()
//    tfidfs.take(20).foreach(println)
    println("=============================================")
    println("Storing the output files (Indexing file + Statistics file + Articles info file) ")
    println("=============================================")
    persist(articlesInfo, docFreqs, tfidfs)
    // persist the indexing result for using in processing queries

    println("=============================================")
    println("The program successfully Ended")
    println("=============================================")
  }

  /**
   * Read the files from the specified URI
   *
   * @param uri uri of folder of files or the file to read, it supports reading from HDFS and local disk
   * @return
   */
  def readInput(uri: String): RDD[(Int, String, String, String)] = {
    val schema = new StructType()
      .add("id", StringType, nullable = false)
      .add("url", StringType, nullable = false)
      .add("title", StringType, nullable = false)
      .add("text", StringType, nullable = false)

    val rdd = spark
      .read
      .schema(schema)
      .json(uri)
      .rdd

    val articles = rdd
      .map(
        row => (
          row.getString(0).toInt, // ID
          row.getString(1), // URL
          row.getString(2), // TITLE
          row.getString(3) // TEXT
        )
      )
    articles
  }

  def splitFilterText(text: String): Array[String] = {
    text.toLowerCase.split("[\n \t,.?!\"\'()<>;]").filter(_.length > 0)
  }

  def enumerateWordsByArticle(textCorpus: RDD[(Int, String)]): RDD[(Int, Array[String])] = {
    textCorpus
      .map(a => (a._1, splitFilterText(a._2)))
    // Output: ((article id, Array[word]))
  }

  def getWordFreq(wordsByArticle: RDD[(Int, Array[String])]): RDD[((Int, String), Int)] = {
    wordsByArticle // words per article - (article_id, Array[word])
      .flatMapValues(word_articleid => word_articleid) // flatting the words - (article_id, word)
      .map(word_articleid => (word_articleid, 1)) // mapping to (word, 1) for counting purpose - (article_id, word , 1)
      .reduceByKey(_ + _) // grouping by document and word - ((article_id, word), word freq)
      .map(
        word_articleid_1 => // (word , article_id, 1)
        {
          val word_articleid = word_articleid_1._1 // just (articleid, word)
          val word_freq = word_articleid_1._2 // (word_freq)
//          Map(word_articleid -> word_freq) // calculating word frequency in each article
          (word_articleid , word_freq) // calculating word frequency in each article
        }
      )
    // Output: Map((article id, word) -> word_freq)
  }

  def getTotalWordsPerArticle(wordFreqs: RDD[((Int, String), Int)]): RDD[((Int, String), (Int, Int))] = {
    val t = wordFreqs
      .map(a=>(a._1._1, (a._1._2,a._2))) // mapping to (article id, (word, wd_freq)) to make the article id as the key

    val t2 = t
      .map(a=> (a._1, a._2._2))
      .reduceByKey(_ + _) // reducing by article id to get total words for each article

    val t3 = t
      .join(t2) // we combine the results by join operation
      .map(a => ((a._1, a._2._1._1), (a._2._1._2, a._2._2)))
    t3
    // Output: Map((article id, word) -> (word_freq, total_words_per_article))
  }

  def getArticleFreqPerTerm(totalWordsPerArticle: RDD[((Int, String), (Int, Int))]): RDD[((String, Int), (Int, Int, Int))] = {
    // Input: Map((article id, word) -> (word_freq, total_words_per_article))
    totalWordsPerArticle
      .map(a => (a._1._2, (a._1._1, a._2._1, a._2._2))) // mapping to (word , (article id, word_freq, total_words_per_article)) to make the word as the key
      .groupByKey() // group be term
      .map(a => ((a._1, a._2.size), a._2)) // computing the article frequency per term

    //  ( (word, article_freq_per_term) , Iterable[(article id, word_freq, total_words_per_article)])
      .flatMapValues(a => a) // flatting the iterable
    // ( (word, article_freq_per_term) , (article id, word_freq, total_words_per_article))
      .map(a => ((a._1._1, a._2._1), (a._2._2, a._2._3, a._1._2))) // ((article id, word),(word_freq, total_words_per_article, article_freq_per_term))
      .sortByKey() // sort by (word, article id)
    // Output: ((word, article id),(word_freq, total_words_per_article, article_freq_per_term))
  }

  def getTFIDFWeights(docFreqs: RDD[((String, Int), (Int, Int, Int))]): RDD[((String, Int), Double)] = {
    // Input: ((word, article id),(word_freq, total_words_per_article, article_freq_per_term))
    docFreqs
      .map(a => (a._1, (a._2._1.toDouble / a._2._2.toDouble) * Math.log10(totalArticles.toDouble / a._2._3.toDouble)))
    // weight = TF * IDF
    // TF = word_freq / total_words_per_article
     // IDF = Math.log(totalArticles / article_freq_per_term)
    // Output: ((word, article id), weight)
  }

  def persist(articleInfo: RDD[(Int, String, String)],
              docFreqs: RDD[((String, Int), (Int, Int, Int))],
              tfidfs: RDD[((String, Int), Double)]
             ): Unit = {

    val rowArticleInfo = articleInfo.map(a => Row(a._1, a._2, a._3))
    val rowDocFreqs = docFreqs.map(a => Row(a._1._1, a._1._2, a._2._1, a._2._2, a._2._3))
    val rowTfIdf = tfidfs.map(a => Row(a._1._1, a._1._2, a._2))

    println("=============================================")
    println("Persisting article info file.....")
    println("=============================================")
    spark.createDataFrame(rowArticleInfo, articleInfoSchema).write.mode(SaveMode.Overwrite).save(articleInfoSavePath)


    println("=============================================")
    println("Persisting statistics file.....")
    println("=============================================")
    spark.createDataFrame(rowDocFreqs, docFreqsSchema).write.mode(SaveMode.Overwrite).save(docWordFreqSavePath)

    println("=============================================")
    println("Statistics will be saved in another text file for viewing purposes ==> Path:>>  " + viewIndexingSavePath )
    println("=============================================")
    spark.createDataFrame(rowDocFreqs, docFreqsSchema).write.mode(SaveMode.Overwrite).csv(viewIndexingSavePath)

    println("=============================================")
    println("Persisting weights file.....")
    println("=============================================")
    spark.createDataFrame(rowTfIdf, indexingSchema).write.mode(SaveMode.Overwrite).save(indexingSavePath)

  }

  def spinUpSpark(): SparkSession ={
    val spark:SparkSession = SparkSession
      .builder()
      .appName("Text Indexer/Retriever")
//            .master("spark://namenode:9000")
      .master("local[*]")
//      .master("yarn")
      .getOrCreate()
    //  import spark.implicits._
    spark
  }

  val articleInfoSchema: StructType = new StructType()
    .add("id", IntegerType, nullable = false)
    .add("url", StringType, nullable = false)
    .add("title", StringType, nullable = false)

  val docFreqsSchema: StructType =
    new StructType()
      .add("term", StringType, nullable = false)
      .add("article_id", IntegerType, nullable = false)
      .add("word_frequency", IntegerType, nullable = false)
      .add("term_frequency", IntegerType, nullable = false)
      .add("doc_frequency", IntegerType, nullable = false)

  val indexingSchema: StructType = new StructType()
    .add("term", StringType, nullable = false)
    .add("article_id", IntegerType, nullable = false)
    .add("weight", DoubleType, nullable = false)
}
//SPARK_LOCAL_IP=10.91.50.206 spark-submit --master yarn --class Indexer firasjolhabigdataassignment1_2.12-0.1.jar hdfs://namenode:9000/EnWikiMedium/*

//SPARK_LOCAL_IP=10.91.50.206 spark-submit --master yarn --class Ranker firasjolhabigdataassignment1_2.12-0.1.jar "what types of sports played with ball" 20 Product