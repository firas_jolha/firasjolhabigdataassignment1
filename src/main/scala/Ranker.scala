import Indexer._
import org.apache.spark.rdd.RDD

import scala.collection.immutable.Map


object Ranker {

  // The query
  var query: String = "the"

  var numRetrievedDocs = 10
  var rankingMetric: RANKING_METRICS.Value = RANKING_METRICS.BM_25
  var weights: RDD[((String, Int), Double)] = _
  var docWordFreq: RDD[((String, Int), (Int, Int, Int))] = _
  var articleInfo: Map[Int, ( String, String)] = _
  // number of documents/articles in the collection
  var docsNum: Int = _
  // length of each article (article id, length)
  var docsLength: RDD[(Int, Int)] = _
  // average length of all documents in the collection
  var docsAverage: Double = _

  /**
   * The entry point of our Ranker
   * @param args the program arguments including specifying the query, number of retrieved articles and ranking function
   */
  def main(args: Array[String]): Unit = {

    init() // read persisted files from indexing stage

    // Processing the program arguments
    if (args.length > 0) {
      try {
        if (args(0).nonEmpty)
          query = args(0)
        numRetrievedDocs = args(1).toInt
        rankingMetric =
          if (args(2).toLowerCase.startsWith("p"))
            RANKING_METRICS.Product
          else
            RANKING_METRICS.BM_25

        println("Query to process '" + query + "'")
        println("Number of documents to retrieve " + numRetrievedDocs)
        println("Ranking metric " + rankingMetric.toString)

        // Process the query
        val queryTerms: RDD[(String, Int)] = processQuery(query)

        // Compute retrieval results according to a specific ranking function
        val rankingResults: RDD[(Int, Double)] = {
          rankingMetric match {
            case RANKING_METRICS.Product => computeRelevance(queryTerms)
            case _ => computeBM25(queryTerms)
          }
        }
        // We select top numnber of retrieved articles
        // We filter results to remove results which have zero relevance
        val results = rankingResults
          .map(_.swap)
          .top(numRetrievedDocs)
          .filter(a => a._1 > 0.0)

          // If no results found, maybe all of the relevance values are zero
          if (results.length==0) println("No Relevant Articles Found!")
          else
            results
              .foreach(
                a => // Printing the results
                {
                  println(a._2," | " , articleInfo(a._2)._1, " | ", articleInfo(a._2)._2, " | ", a._1)
                }
              )
      } catch {
        case e: Exception =>
          println("===================================")
          println("Error message => " + e.getMessage)
          printCommandInfo() // usage info
      }
    } else {
      printCommandInfo()
    }
  }


  def init(): Unit = {

    // read weights from persisted files and put it in the correct schema
    weights = spark.read.schema(indexingSchema)
      .parquet(indexingSavePath).rdd
      .map(a => ((a.getString(0), a.getInt(1)), a.getDouble(2)))

    // read docWordFreq from persisted files and put it in the correct schema
    docWordFreq = spark.read.schema(docFreqsSchema)
      .parquet(docWordFreqSavePath).rdd
      .map(a => ((a.getString(0), a.getInt(1)), (a.getInt(2), a.getInt(3), a.getInt(4))))

    // read articleInfo from persisted file and put it in the correct schema
    articleInfo = spark.read.schema(articleInfoSchema)
      .parquet(articleInfoSavePath).rdd
      .map(a => (
        a.getAs[Int]("id"),
        (a.getAs[String]("url"),
        a.getAs[String]("title")
        )
        )
      ).collect().toMap


    // Number of documents in the collection
    docsNum = articleInfo.size

    // the length of each document (document id, length)
    // number of words in each document
    docsLength = computeDocsLength()

    // Average length of documents in the collection
    docsAverage = computeAverageDocsLength(docsLength)
  }

  def processQuery(query: String): RDD[(String, Int)] = {
    // enumerate words of the query
    val words = spark.sparkContext.parallelize(
      splitFilterText(query)
        .toSeq
    )
    words
      .groupBy(word => word)
      .map(word => (word._1, word._2.size))
  }

  def computeQueryWeights(queryWords: RDD[(String, Int)]): Map[String, Double] = {
    // weight = TF * IDF
    // TF = word_freq / total_words_per_article
    // IDF = Math.log(totalArticles / article_freq_per_term)
    // Output: ((article id, word), weight)
    val queryTerms:Set[String] = queryWords.map(_._1).distinct().collect().toSet
    // first we get query terms
    // then we filter the weights for only these query terms

    docWordFreq
      .map(a => (a._1._1, (a._1._2, a._2._1, a._2._2, a._2._3))) // (word, (article id, word_freq, total_words_per_article, article_freq_per_term))
      .filter(a => queryTerms.contains(a._1)) // (word, (article id, word_freq, total_words_per_article, article_freq_per_term))
      .map( // (word, (article id, word_freq, total_words_per_article, article_freq_per_term))
        a => (a._1, (a._2._2.toDouble / a._2._3.toDouble) * Math.log(docsNum.toDouble / a._2._4.toDouble))
      )
      .collectAsMap().toMap
  }

  def computeDocsLength(): RDD[(Int, Int)] = {
    docWordFreq.map(a => (a._1._2, a._2._2)) // the length is the total number words in the document
      .distinct()
  }

  def computeAverageDocsLength(docsLength: RDD[(Int, Int)]): Double = {
    docsLength.map(_._2).reduce(_ + _) / docsNum.toDouble
  }

  def computeRelevance(query: RDD[(String, Int)]): RDD[(Int, Double)] = {

    // We compute the weight of each query term
    val queryWeights = computeQueryWeights(query)
    // we intersect the terms of query with terms of each document to compute the dot product
    val queryDocsRelevance: RDD[(Int, Double)] =
      weights
        .map(a => (a._1._2, (a._1._1, a._2)))
        .groupByKey()
        .map(a => (a._1, a._2.toMap))
        .map(a => {
          val docWeights = a._2
          (
            a._1,
            docWeights.keySet.intersect(queryWeights.keySet)
              .map(k => (docWeights(k), queryWeights(k)))
              .toMap
          )
        }
        )
        .map(a => (a._1, a._2.map(d => d._1 * d._2).sum))

    queryDocsRelevance
  }

  def computeBM25(query: RDD[(String, Int)]): RDD[(Int, Double)] = {
    val k1 = 2.0
    val b = 0.75
    val len: RDD[(Int, Int)] = docsLength // (article id, article length)
    val avg:Double = docsAverage
    val queryTerms = query.map(_._1).distinct().collect().toSet

    val queryTermFreq: RDD[((String, Int), Int)] =
      docWordFreq
        .map(a => ((a._1._1, a._1._2), a._2._1))
        .filter(a=>queryTerms.contains(a._1._1))

    val queryIDF: RDD[(String, Double)] =
      docWordFreq.map(a => (a._1._1, a._2._3)) // (word, article_freq_per_term))
        .distinct()
        .filter(a=>queryTerms.contains(a._1))
        .map(a => (a._1, Math.log10(1 + (docsNum - a._2 + 0.5) / (a._2 + 0.5))))

    val temp:RDD[(Int, Double)] = len.map(a => (a._1, k1 * (1 - b + b * a._2 / avg)))

    val temp2:RDD[(Int, String, Double)] = queryTermFreq
      .map(a=>(a._1._2, (a._1._1, a._2))) // (article id, (word, word_freq))
      .join(temp) // (article id, ((word, word_freq), temp_value))
      .map(a=>(a._1, a._2._1._1 ,(a._2._1._2 * (k1+1)) / (a._2._1._2 + a._2._2)))
    //  (article id, word, fraction_value)

    // the value of fraction
    val bm25Values: RDD[(Int, Double)] = temp2
      .map(a => (a._2, (a._1, a._3))) // (word, (article id, fraction_value))
      .join(queryIDF) // (word, ((article id, fraction_value), idf))
      .map(a => (a._2._1._1, a._2._1._2 * a._2._2)) // (article id, score_i)
      .reduceByKey(_ + _) // // // (article id, score)

    bm25Values
  }

  def printCommandInfo(): Unit = {
    println("The Ranker should specify the following params \n" +
      "\"Query\"  num_of_retrieved_docs  ranking_metric  \n\n" +
      "The \"Query\" will be processed and the num_of_retrieved_docs{default=10}\n" +
      " will be returned and the ranking method BM(default) or Product will be used\n\n" +
      "Example: \n" +
      "\"I love my country\"   10    BM")
  }
}

// Ranking functions as enum in Scala
object RANKING_METRICS extends Enumeration {
  val BM_25, Product = Value
}